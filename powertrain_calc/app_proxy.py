"""Proxy module to initialize application."""

import os
from typing import Dict

from flask import Flask

from .db import db
from .utils.logging import log_info_before_after


class AppProxy:
    """Interface class to initialize the application."""

    def __init__(self, name: str, test_config: Dict = None):
        self.name = name
        self.config_file = 'config.py'
        self.test_config = test_config

        # create application instance
        self.app = Flask(self.name, instance_relative_config=True)

        # perform steps to initialize
        self._init_config()
        self._init_paths()
        self._init_storage()
        self._init_routes()


    def get_app(self):
        """Retrieve the application instance."""
        return self.app


    @log_info_before_after(
        'Initializing configuration...',
        'Done initializing configuration.'
    )
    def _init_config(self):
        # set default config
        self.app.config.from_mapping(
            SECRET_KEY='dev',
            SQLALCHEMY_DATABASE_URI=db.get_sqlite_uri(self.app, self.name),
            SQLALCHEMY_TRACK_MODIFICATIONS=False
        )

        # update config
        if self.test_config is None:
            self.app.config.from_pyfile(self.config_file, silent=True)
        else:
            self.app.config.from_mapping(self.test_config)


    @log_info_before_after(
        'Initializing file paths...',
        'Done initializing file paths.'
    )
    def _init_paths(self):
        try:
            os.makedirs(self.app.instance_path)
        except OSError:
            pass


    @log_info_before_after(
        'Initializing storage...',
        'Done initializing storage.'
    )
    def _init_storage(self):
        db.db.init_app(self.app)
        self.app.cli.add_command(db.init_db_command)


    @log_info_before_after(
        'Initializing URL routes...',
        'Done initializing URL routes.'
    )
    def _init_routes(self):
        from .controllers.home import blueprint as home_controller
        from .controllers.vehicle import blueprint as vehicle_controller
        from .controllers.engine import blueprint as engine_controller
        from .controllers.transmission import blueprint as transmission_controller
        from .controllers.tire import blueprint as tire_controller

        self.app.register_blueprint(home_controller)
        self.app.register_blueprint(vehicle_controller)
        self.app.register_blueprint(engine_controller)
        self.app.register_blueprint(transmission_controller)
        self.app.register_blueprint(tire_controller)

        self.app.add_url_rule('/', endpoint='index')

        from .models.models import fill_db_command
        self.app.cli.add_command(fill_db_command)
