"""External functions related to database."""

import os

import click

from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def get_sqlite_uri(app, name):
    """Retrieve URI used by SQLite database."""
    db_path = os.path.join(app.instance_path, f'{name}.sqlite').replace('\\', '\\\\')
    db_uri = f'sqlite:///{db_path}'
    return db_uri


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Click command to initialize the database."""
    init_db()
    click.echo('Initialized the database.')


def init_db():
    """Clear the existing data and create new tables."""
    db.drop_all()
    db.create_all()
