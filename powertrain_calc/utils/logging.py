"""Utility module for common logging functions."""

from functools import wraps
import logging


def log_exception():
    """Decorator to log any exception from a function call"""
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                logging.exception(f'There was an exception in {func.__name__}')
                raise
        return wrapper
    return decorator


def log_info_before_after(m_before: str, m_after: str):
    """Decorator to log message before and after a function call"""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            logging.info(f'{func.__module__}\t{func.__name__}\t{m_before}')
            func(*args, **kwargs)
            logging.info(f'{func.__module__}\t{func.__name__}\t{m_after}')
        return wrapper
    return decorator
