var config = {
    type: 'line',
    data: {
        datasets: []
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'x',
            callbacks: {
                label: function(tooltipItem, data) {
                    var x = tooltipItem.xLabel.toFixed(2).padStart(6, '\u2007');
                    var y = tooltipItem.yLabel.toLocaleString('en');
                    var label = data.datasets[tooltipItem.datasetIndex].label;
                    return label + ": " + x + " kph @ " + y + " rpm";
                }
            }
        },
        scales: {
            xAxes: [{
                type: 'linear',
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Vehicle speed (kph)'
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: 10,
                    suggestedMax: 210,
                }
            }],
            yAxes: [{
                type: 'linear',
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Engine RPM'
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: 1000,
                    suggestedMax: 7500,
                }
            }]
        }
    }
};

function convertDatasets(data) {
    datasets = [];

    for(var i = 0; i < data.length; i++) {
        var dataset = {
            cubicInterpolationMode: 'linear',
            label: data[i].label,
            borderWidth: 1,
            borderColor: data[i].color,
            pointBackgroundColor: data[i].color,
            backgroundColor: 'rgba(0, 0, 0, 0)',
            data: data[i].data
        };
        datasets.push(dataset);
    }
    return datasets;
}

function updateChart(initialRender) {
    // get current selected value of dropdown
    var dropdown = document.getElementById("myDropdown");
    var dropdownToggle = dropdown.getElementsByClassName("dropdown-toggle")[0];
    var vehicleName = dropdownToggle.textContent.trim();

    var dropdownItems = dropdown.getElementsByClassName("dropdown-item")
    var vehicleId = 0;
    for(var i = 0; i < dropdownItems.length; i++) {
        if(dropdownItems[i].textContent.trim() == vehicleName) {
            vehicleId = dropdownItems[i].getAttribute("data-value")
            break;
        }
    }

    // send request
    var request = new XMLHttpRequest();
    request.open("GET", "/api/vehicle/" + vehicleId + "/datasets?extended")
    request.setRequestHeader("Content-type", "application/json");
    request.onreadystatechange = function() {
        if(request.readyState == XMLHttpRequest.DONE && request.status == 200) {
            // set chart dataset
            response = JSON.parse(request.responseText);
            config.data.datasets = convertDatasets(response);

            // trigger chart rendering
            if(initialRender === true) {
                // create chart
                var ctx = document.getElementById("myChart").getContext('2d');
                window.myChart = new Chart(ctx, config);
            } else {
                // re-render chart
                window.myChart.update();
            }
        }
    }
    request.send()
}

window.onload = function() {
    updateChart(true);
};

function updateDropdownOnClick(element) {
    // set current selected value of dropdown
    var dropdown = document.getElementById("myDropdown");
    var dropdownToggle = dropdown.getElementsByClassName("dropdown-toggle")[0];
    dropdownToggle.textContent = element.textContent;

    // update chart
    updateChart(false);
}
