"""Application driver."""

from .app_proxy import AppProxy


def create_app(test_config=None):
    """Initialize application."""
    launcher = AppProxy("powertrain_calc", test_config=test_config)

    return launcher.get_app()
