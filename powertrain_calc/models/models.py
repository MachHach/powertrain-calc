"""External functions related to data models."""

from typing import List

import click
from flask.cli import with_appcontext

from .engine import Engine
from .transmission import Transmission, TransmissionRatio
from .tire import Tire
from .vehicle import Vehicle
from ..db.db import db


@click.command('fill-db')
@with_appcontext
def fill_db_command():
    """Click command to fill the database."""
    fill_db()
    click.echo('Filled the database.')


def fill_db():
    """Populate database with sample data."""
    vehicle1 = fill_vehicle('Vios 1.5G AT (NCP42)', 'Toyota', 'rgba(120, 120, 120, 1)',
        '1NZ-FE', 'Toyota', 4200, 6000,
        'U340E', 'Toyota', [2.847, 1.552, 1.000, 0.700], 2.343, 3.924,
        195, 55, 15)
    vehicle2 = fill_vehicle('Vios 1.5J MT (NSP151)', 'Toyota', 'rgba(0, 0, 0, 1)',
        '2NR-FE', 'Toyota', 4200, 6000,
        'C150', 'Toyota', [3.545, 1.904, 1.310, 0.969, 0.815], 3.250, 4.058,
        185, 60, 15)
    vehicle3 = fill_vehicle('Civic 1.8 AT (FD1)', 'Honda', 'rgba(50, 0, 100, 1)',
        'R18A', 'Honda', 4300, 6300,
        '5AT', 'Honda', [2.666, 1.534, 1.022, 0.721, 0.525], 1.957, 4.440,
        205, 55, 16)
    vehicle4 = fill_vehicle('86 AT (ZN6)', 'Toyota', 'rgba(200, 100, 100, 1)',
        'FA20D', 'Subaru', 6400, 7000,
        'A960E', 'Toyota', [3.538, 2.060, 1.404, 1.000, 0.713, 0.582], 3.168, 4.100,
        215, 45, 17)
    vehicle5 = fill_vehicle('86 MT (ZN6)', 'Toyota', 'rgba(100, 100, 200, 1)',
        'FA20D', 'Subaru', 6400, 7000,
        'TL70', 'Toyota', [3.626, 2.188, 1.541, 1.213, 1.000, 0.767], 3.437, 4.100,
        215, 45, 17)
    vehicle6 = fill_vehicle('Civic Type-R (FK8)', 'Honda', 'rgba(200, 0, 0, 1)',
        'K20C1', 'Honda', 2500, 6500,
        '6MT', 'Honda', [3.625, 2.115, 1.529, 1.125, 0.911, 0.735], 3.758, 4.111,
        245, 30, 20)

    db.session.add(vehicle1)
    db.session.add(vehicle2)
    db.session.add(vehicle3)
    db.session.add(vehicle4)
    db.session.add(vehicle5)
    db.session.add(vehicle6)
    db.session.commit()


def fill_vehicle(vehicle_name: str, vehicle_brand: str, vehicle_color: str,
        engine_name: str, engine_brand: str, engine_max_torque_rpm: int, engine_max_power_rpm: int,
        trans_name: str, trans_brand: str, trans_forward_ratios: List[float], trans_reverse_ratio: float, trans_final_drive_ratio: float,
        tire_width: int, tire_aspect_ratio: int, tire_wheel_radius: int) -> Vehicle:
    """Create vehicle data."""
    vehicle = Vehicle(name=vehicle_name, brand=vehicle_brand, color=vehicle_color)

    Engine(name=engine_name, brand=engine_brand, max_torque_rpm=engine_max_torque_rpm, max_power_rpm=engine_max_power_rpm, vehicles=[vehicle])

    Transmission(name=trans_name, brand=trans_brand, reverse_ratio=trans_reverse_ratio, final_drive_ratio=trans_final_drive_ratio,
        forward_ratios=[TransmissionRatio(gear_order=i+1, forward_ratio=r) for i, r in enumerate(trans_forward_ratios)], vehicles=[vehicle])

    Tire(width=tire_width, aspect_ratio=tire_aspect_ratio, wheel_radius=tire_wheel_radius, vehicles=[vehicle])

    return vehicle
