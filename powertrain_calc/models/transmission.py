"""Transmission model."""

import datetime

from ..db.db import db


class Transmission(db.Model):
    """Transmission model."""
    # Database attributes
    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    name = db.Column(db.Text, nullable=False)
    brand = db.Column(db.Text, nullable=False)
    reverse_ratio = db.Column(db.Float, nullable=False)
    final_drive_ratio = db.Column(db.Float, nullable=False)

    # Database relationships
    vehicles = db.relationship('Vehicle', backref='transmission', lazy=True)
    forward_ratios = db.relationship('TransmissionRatio', backref='transmission', lazy=True)

    # Declarative attributes
    @property
    def initial_forward_ratios(self):
        """Transmission ratios before final drive adjustment."""
        return [r.forward_ratio for r in self.forward_ratios]

    @property
    def final_forward_ratios(self):
        """Transmission ratios after final drive adjustment."""
        return [r * self.final_drive_ratio for r in self.initial_forward_ratios]

    @property
    def final_reverse_ratio(self):
        """Transmission reverse gear ratio after final drive adjustment."""
        return self.reverse_ratio * self.final_drive_ratio

    def __repr__(self):
        return f'<Transmission {self.brand!r} {self.name!r}>'

    @property
    def serialize(self):
        """Serialize the object as dictionary."""
        return {
            'id': self.id,
            'name': self.name,
            'brand': self.brand,
            'reverse_ratio': self.reverse_ratio,
            'final_drive_ratio': self.final_drive_ratio,
            'created_datetime': self.created_datetime.isoformat()
        }

    @property
    def serialize_all(self):
        """Serialize the object as dictionary, including declarative attributes."""
        base_dict = self.serialize
        base_dict['initial_forward_ratios'] = self.initial_forward_ratios
        base_dict['final_forward_ratios'] = self.final_forward_ratios
        base_dict['final_reverse_ratio'] = self.final_reverse_ratio
        return base_dict


class TransmissionRatio(db.Model):
    """Transmission Ratio model."""
    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    gear_order = db.Column(db.Integer, nullable=False)
    forward_ratio = db.Column(db.Float, nullable=False)

    transmission_id = db.Column(db.Integer, db.ForeignKey('transmission.id'), nullable=False)

    def __repr__(self):
        return f'<TransmissionRatio {self.gear_order!r} {self.forward_ratio!r}>'

    @property
    def serialize(self):
        """Serialize the object as dictionary."""
        return {
            'id': self.id,
            'gear_order': self.gear_order,
            'forward_ratio': self.forward_ratio,
            'created_datetime': self.created_datetime.isoformat()
        }
