"""Vehicle model."""

import datetime

from ..db.db import db


class Vehicle(db.Model):
    """Vehicle model."""
    # Database attributes
    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    name = db.Column(db.Text, nullable=False)
    brand = db.Column(db.Text, nullable=False)
    color = db.Column(db.Text, nullable=False)

    # Database relationships
    engine_id = db.Column(db.Integer, db.ForeignKey('engine.id'), nullable=False)
    transmission_id = db.Column(db.Integer, db.ForeignKey('transmission.id'), nullable=False)
    tire_id = db.Column(db.Integer, db.ForeignKey('tire.id'), nullable=False)

    # Declarative attributes
    @property
    def max_torque_speeds_kph(self):
        """Vehicle instantaneous KPH (km/h) at max engine torque, for each gear."""
        engine_kph = self.engine.max_torque_rph
        trans_ratio = self.transmission.final_forward_ratios
        tire_circumference = self.tire.circumference_km
        return [(engine_kph * tire_circumference) / r for r in trans_ratio]

    @property
    def max_power_speeds_kph(self):
        """Vehicle instantaneous KPH (km/h) at max engine power, for each gear."""
        engine_kph = self.engine.max_power_rph
        trans_ratio = self.transmission.final_forward_ratios
        tire_circumference = self.tire.circumference_km
        return [(engine_kph * tire_circumference) / r for r in trans_ratio]

    def __repr__(self):
        return f'<Vehicle {self.brand!r} {self.name!r}>'

    @property
    def serialize(self):
        """Serialize the object as dictionary."""
        return {
            'id': self.id,
            'name': self.name,
            'brand': self.brand,
            'color': self.color,
            'created_datetime': self.created_datetime.isoformat()
        }

    @property
    def serialize_all(self):
        """Serialize the object as dictionary, including declarative attributes."""
        base_dict = self.serialize
        base_dict['engine_id'] = self.engine_id
        base_dict['transmission_id'] = self.transmission_id
        base_dict['tire_id'] = self.tire_id
        base_dict['max_torque_speeds_kph'] = self.max_torque_speeds_kph
        base_dict['max_power_speeds_kph'] = self.max_power_speeds_kph
        return base_dict

    @property
    def datasets(self):
        """Build dataset for RPM against KPH."""
        datasets = []
        max_torque_rpm = self.engine.max_torque_rpm
        max_power_rpm = self.engine.max_power_rpm

        for i, ratio in enumerate(self.transmission.forward_ratios):
            dataset = {
                'color': self.color,
                'label': f'{self.name}, gear {ratio.gear_order} ({ratio.forward_ratio})',
                'data': [
                    {'x': 0, 'y': 0},
                    {'x': self.max_torque_speeds_kph[i], 'y': max_torque_rpm},
                    {'x': self.max_power_speeds_kph[i], 'y': max_power_rpm}
                ]
            }
            datasets.append(dataset)

        return datasets

    @property
    def extended_datasets(self):
        """Build dataset for RPM against KPH, with extended data."""
        datasets = self.datasets

        # get all related KPH (x-axis) values
        x_values = {data_point['x'] for dataset in datasets for data_point in dataset['data']}

        # generate extended data
        for dataset in datasets:
            data_points = dataset['data']
            data_points_x = [data_point['x'] for data_point in data_points]

            first_point = data_points[0]
            last_point = data_points[-1]
            # the graph is linear with zero constant (y=mx+c where c=0), i.e. m=y/x
            gradient = (last_point['y'] - first_point['y']) / (last_point['x'] - first_point['x'])

            for x_value in x_values:
                if x_value not in data_points_x and x_value < last_point['x']:
                    data_points.append({
                        'x': x_value,
                        'y': gradient * x_value
                    })

            data_points.sort(key=lambda k: k['x'])

        return datasets
