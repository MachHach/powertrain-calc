"""Tire model."""

import datetime
from math import pi

from .common import INCH_TO_MM, MM_PER_KM, FRACTION_PERCENT
from ..db.db import db


class Tire(db.Model):
    """Tire model."""
    # Database attributes
    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    width = db.Column(db.Integer, nullable=False)
    aspect_ratio = db.Column(db.Integer, nullable=False)
    wheel_radius = db.Column(db.Integer, nullable=False)

    # Database relationships
    vehicles = db.relationship('Vehicle', backref='tire', lazy=True)

    # Declarative attributes
    @property
    def aspect_ratio_fraction(self):
        """Tire aspect ratio measured as decimal fraction."""
        return self.aspect_ratio / FRACTION_PERCENT

    @property
    def wheel_radius_mm(self):
        """Wheel radius measured as mm (millimetres)."""
        return self.wheel_radius * INCH_TO_MM

    @property
    def sidewall_height_mm(self):
        """Tire sidewall height measured as mm (millimetres)."""
        return self.width * self.aspect_ratio_fraction

    @property
    def diameter_mm(self):
        """Tire diameter measured as mm (millimetres)."""
        return self.wheel_radius_mm + (2 * self.sidewall_height_mm)

    @property
    def circumference_mm(self):
        """Tire circumference measured as mm (millimetres)."""
        return self.diameter_mm * pi

    @property
    def circumference_km(self):
        """Tire circumference measured as km (kilometres)."""
        return self.circumference_mm / MM_PER_KM

    def __repr__(self):
        return f'<Tire {self.width!r} {self.aspect_ratio!r} {self.wheel_radius!r}>'

    @property
    def serialize(self):
        """Serialize the object as dictionary."""
        return {
            'id': self.id,
            'width': self.width,
            'aspect_ratio': self.aspect_ratio,
            'wheel_radius': self.wheel_radius,
            'created_datetime': self.created_datetime.isoformat()
        }

    @property
    def serialize_all(self):
        """Serialize the object as dictionary, including declarative attributes."""
        base_dict = self.serialize
        base_dict['aspect_ratio_fraction'] = self.aspect_ratio_fraction
        base_dict['wheel_radius_mm'] = self.wheel_radius_mm
        base_dict['sidewall_height_mm'] = self.sidewall_height_mm
        base_dict['diameter_mm'] = self.diameter_mm
        base_dict['circumference_mm'] = self.circumference_mm
        base_dict['circumference_km'] = self.circumference_km
        return base_dict
