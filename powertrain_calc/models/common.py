"""Common functions and constants for models."""

# Constants: General
FRACTION_PERCENT = 100

# Constants: Time
MINUTES_PER_HOUR = 60

# Constants: Distance
INCH_TO_MM = 25.4
MM_PER_KM = 1000000
