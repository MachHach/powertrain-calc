"""Engine model."""

import datetime

from .common import MINUTES_PER_HOUR
from ..db.db import db


class Engine(db.Model):
    """Engine model."""
    # Database attributes
    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    name = db.Column(db.Text, nullable=False)
    brand = db.Column(db.Text, nullable=False)
    max_torque_rpm = db.Column(db.Integer, nullable=False)
    max_power_rpm = db.Column(db.Integer, nullable=False)

    # Database relationships
    vehicles = db.relationship('Vehicle', backref='engine', lazy=True)

    # Declarative attributes
    @property
    def max_torque_rph(self):
        """Engine RPH (revolutions per hour) at max torque."""
        return self.max_torque_rpm * MINUTES_PER_HOUR

    @property
    def max_power_rph(self):
        """Engine RPH (revolutions per hour) at max power."""
        return self.max_power_rpm * MINUTES_PER_HOUR

    def __repr__(self):
        return f'<Engine {self.brand!r} {self.name!r}>'

    @property
    def serialize(self):
        """Serialize the object as dictionary."""
        return {
            'id': self.id,
            'name': self.name,
            'brand': self.brand,
            'max_torque_rpm': self.max_torque_rpm,
            'max_power_rpm': self.max_power_rpm,
            'created_datetime': self.created_datetime.isoformat()
        }

    @property
    def serialize_all(self):
        """Serialize the object as dictionary, including declarative attributes."""
        base_dict = self.serialize
        base_dict['max_torque_rph'] = self.max_torque_rph
        base_dict['max_power_rph'] = self.max_power_rph
        return base_dict
