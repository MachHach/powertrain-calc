"""Engine API controller."""

from http import HTTPStatus

from flask import (
    Blueprint, jsonify, request, make_response, url_for
)
from werkzeug.exceptions import abort

from .common import should_get_all_attr, Validator
from ..db.db import db
from ..models.engine import Engine


blueprint = Blueprint('engine', __name__, url_prefix='/api/engine')


@blueprint.route('/', methods=('GET',))
def route_get_engines():
    """Retrieve all Engine objects."""
    engines = Engine.query.all()

    dict_obj = [e.serialize_all if should_get_all_attr(request) else e.serialize for e in engines]

    return jsonify(dict_obj)


@blueprint.route('/', methods=('POST',))
def route_create_engine():
    """Create a new Engine object."""
    validator = Validator(request)
    validator.check_form_param('name')
    validator.check_form_param('brand')
    validator.check_form_param('max_torque_rpm', expect_type=int)
    validator.check_form_param('max_power_rpm', expect_type=int)

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    engine = Engine(
        name=request.form['name'],
        brand=request.form['brand'],
        max_torque_rpm=int(request.form['max_torque_rpm']),
        max_power_rpm=int(request.form['max_power_rpm']))
    db.session.add(engine)
    db.session.commit()

    res = make_response(jsonify(engine.serialize), HTTPStatus.CREATED)
    res.headers['Location'] = url_for('engine.route_get_engine', e_id=engine.id)

    return res


@blueprint.route('/<int:e_id>', methods=('GET',))
def route_get_engine(e_id):
    """Retrieve an Engine object with specified ID."""
    engine = Engine.query.get(e_id)

    if engine is None:
        abort(HTTPStatus.NOT_FOUND, f"Engine of id {e_id} does not exist.")

    dict_obj = engine.serialize_all if should_get_all_attr(request) else engine.serialize

    return jsonify(dict_obj)


@blueprint.route('/<int:e_id>', methods=('PUT',))
def route_update_engine(e_id):
    """Update an Engine object with specified ID."""
    engine = Engine.query.get(e_id)

    if engine is None:
        abort(HTTPStatus.NOT_FOUND, f"Engine of id {e_id} does not exist.")

    validator = Validator(request)

    if 'name' in request.form and validator.check_form_param('name'):
        engine.name = request.form['name']
    if 'brand' in request.form and validator.check_form_param('brand'):
        engine.brand = request.form['brand']
    if 'max_torque_rpm' in request.form and validator.check_form_param('max_torque_rpm', expect_type=int):
        engine.max_torque_rpm = int(request.form['max_torque_rpm'])
    if 'max_power_rpm' in request.form and validator.check_form_param('max_power_rpm', expect_type=int):
        engine.max_power_rpm = int(request.form['max_power_rpm'])

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    db.session.commit()

    return make_response(jsonify(engine.serialize), HTTPStatus.NO_CONTENT)


@blueprint.route('/<int:e_id>', methods=('DELETE',))
def route_delete_engine(e_id):
    """Delete an Engine object with specified ID."""
    engine = Engine.query.get(e_id)

    if engine is None:
        abort(HTTPStatus.NOT_FOUND, f"Engine of id {e_id} does not exist.")

    db.session.delete(engine)
    db.session.commit()

    return ('', HTTPStatus.NO_CONTENT)
