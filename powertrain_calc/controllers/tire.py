"""Tire API controller."""

from http import HTTPStatus

from flask import (
    Blueprint, jsonify, request, make_response, url_for
)
from werkzeug.exceptions import abort

from .common import should_get_all_attr, Validator
from ..db.db import db
from ..models.tire import Tire


blueprint = Blueprint('tire', __name__, url_prefix='/api/tire')


@blueprint.route('/', methods=('GET',))
def route_get_tires():
    """Retrieve all Tire objects"""
    tires = Tire.query.all()

    dict_obj = [t.serialize_all if should_get_all_attr(request) else t.serialize for t in tires]

    return jsonify(dict_obj)


@blueprint.route('/', methods=('POST',))
def route_create_tire():
    """Create a new Tire object."""
    validator = Validator(request)
    validator.check_form_param('width', expect_type=int)
    validator.check_form_param('aspect_ratio', expect_type=int)
    validator.check_form_param('wheel_radius', expect_type=int)

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    tire = Tire(
        width=int(request.form['width']),
        aspect_ratio=int(request.form['aspect_ratio']),
        wheel_radius=int(request.form['wheel_radius']))
    db.session.add(tire)
    db.session.commit()

    res = make_response(jsonify(tire.serialize), HTTPStatus.CREATED)
    res.headers['Location'] = url_for('tire.route_get_tire', t_id=tire.id)

    return res


@blueprint.route('/<int:t_id>', methods=('GET',))
def route_get_tire(t_id):
    """Retrieve a Tire object with specified ID."""
    tire = Tire.query.get(t_id)

    if tire is None:
        abort(HTTPStatus.NOT_FOUND, f"Tire of id {t_id} does not exist.")

    dict_obj = tire.serialize_all if should_get_all_attr(request) else tire.serialize

    return jsonify(dict_obj)


@blueprint.route('/<int:t_id>', methods=('PUT',))
def route_update_tire(t_id):
    """Update a Tire object with specified ID."""
    tire = Tire.query.get(t_id)

    if tire is None:
        abort(HTTPStatus.NOT_FOUND, f"Tire of id {t_id} does not exist.")

    validator = Validator(request)

    if 'width' in request.form and validator.check_form_param('width', expect_type=int):
        tire.width = int(request.form['width'])
    if 'aspect_ratio' in request.form and validator.check_form_param('aspect_ratio', expect_type=int):
        tire.aspect_ratio = int(request.form['aspect_ratio'])
    if 'wheel_radius' in request.form and validator.check_form_param('wheel_radius', expect_type=int):
        tire.wheel_radius = int(request.form['wheel_radius'])

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    db.session.commit()

    return make_response(jsonify(tire.serialize), HTTPStatus.NO_CONTENT)


@blueprint.route('/<int:t_id>', methods=('DELETE',))
def route_delete_tire(t_id):
    """Delete a Tire object with specified ID."""
    tire = Tire.query.get(t_id)

    if tire is None:
        abort(HTTPStatus.NOT_FOUND, f"Tire of id {t_id} does not exist.")

    db.session.delete(tire)
    db.session.commit()

    return ('', HTTPStatus.NO_CONTENT)
