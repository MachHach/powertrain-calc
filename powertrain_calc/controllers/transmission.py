"""Transmission API controller."""

from http import HTTPStatus

from flask import (
    Blueprint, jsonify, request, make_response, url_for
)
from werkzeug.exceptions import abort

from .common import should_get_all_attr, Validator
from ..db.db import db
from ..models.transmission import Transmission, TransmissionRatio


blueprint = Blueprint('transmission', __name__, url_prefix='/api/transmission')


@blueprint.route('/', methods=('GET',))
def route_get_transmissions():
    """Retrieve all Transmission objects"""
    transmissions = Transmission.query.all()

    dict_obj = [t.serialize_all if should_get_all_attr(request) else t.serialize for t in transmissions]

    return jsonify(dict_obj)


@blueprint.route('/', methods=('POST',))
def route_create_transmission():
    """Create a new Transmission object."""
    validator = Validator(request)
    validator.check_form_param('name')
    validator.check_form_param('brand')
    validator.check_form_param('reverse_ratio', expect_type=float)
    validator.check_form_param('final_drive_ratio', expect_type=float)
    validator.check_form_param_as_list('forward_ratios', trim_elements=True, expect_type=float)

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    forward_ratios = [float(e.strip()) for e in request.form['forward_ratios'].split(',')]

    transmission_ratios = [TransmissionRatio(gear_order=i+1, forward_ratio=r) for i, r in enumerate(forward_ratios)]

    transmission = Transmission(
        name=request.form['name'],
        brand=request.form['brand'],
        reverse_ratio=float(request.form['reverse_ratio']),
        final_drive_ratio=float(request.form['final_drive_ratio']),
        forward_ratios=transmission_ratios)
    db.session.add(transmission)
    db.session.commit()

    res = make_response(jsonify(transmission.serialize), HTTPStatus.CREATED)
    res.headers['Location'] = url_for('transmission.route_get_transmission', t_id=transmission.id)

    return res


@blueprint.route('/<int:t_id>', methods=('GET',))
def route_get_transmission(t_id):
    """Retrieve a Transmission object with specified ID."""
    transmission = Transmission.query.filter_by(id=t_id).first()

    if transmission is None:
        abort(HTTPStatus.NOT_FOUND, f"Transmission of id {t_id} does not exist.")

    dict_obj = transmission.serialize_all if should_get_all_attr(request) else transmission.serialize

    return jsonify(dict_obj)


@blueprint.route('/<int:t_id>', methods=('PUT',))
def route_update_transmission(t_id):
    """Update an Transmission object with specified ID."""
    transmission = Transmission.query.get(t_id)

    if transmission is None:
        abort(HTTPStatus.NOT_FOUND, f"Transmission of id {t_id} does not exist.")

    validator = Validator(request)

    if 'name' in request.form and validator.check_form_param('name'):
        transmission.name = request.form['name']
    if 'brand' in request.form and validator.check_form_param('brand'):
        transmission.brand = request.form['brand']
    if 'reverse_ratio' in request.form and validator.check_form_param('reverse_ratio', expect_type=float):
        transmission.reverse_ratio = float(request.form['reverse_ratio'])
    if 'final_drive_ratio' in request.form and validator.check_form_param('final_drive_ratio', expect_type=float):
        transmission.final_drive_ratio = float(request.form['final_drive_ratio'])
    if 'forward_ratios' in request.form and validator.check_form_param_as_list('forward_ratios', trim_elements=True, expect_type=float):
        for transmission_ratio in transmission.forward_ratios:
            db.session.delete(transmission_ratio)
        forward_ratios = [float(e.strip()) for e in request.form['forward_ratios'].split(',')]
        transmission.forward_ratios = [TransmissionRatio(gear_order=i+1, forward_ratio=r) for i, r in enumerate(forward_ratios)]

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    db.session.commit()

    return make_response(jsonify(transmission.serialize), HTTPStatus.NO_CONTENT)


@blueprint.route('/<int:t_id>', methods=('DELETE',))
def route_delete_transmission(t_id):
    """Delete an Transmission object with specified ID."""
    transmission = Transmission.query.get(t_id)

    if transmission is None:
        abort(HTTPStatus.NOT_FOUND, f"Transmission of id {t_id} does not exist.")

    for transmission_ratio in transmission.forward_ratios:
        db.session.delete(transmission_ratio)
    db.session.delete(transmission)
    db.session.commit()

    return ('', HTTPStatus.NO_CONTENT)
