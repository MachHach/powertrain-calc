"""Home page controller."""

from flask import Blueprint, render_template

from ..models.models import Vehicle


blueprint = Blueprint('home', __name__)


@blueprint.route('/')
def index():
    """Index page."""
    vehicles = Vehicle.query.all()

    return render_template('index.html', vehicles=vehicles)
