"""Common functions and constants for controllers."""

from flask import request


def should_get_all_attr(req: request):
    """Whether the current request contains 'mode' query parameter of value 'all'"""
    return req.args.get('mode') == 'all'


def is_possible_type(input_str: str, expect_type: type):
    """
    Validates the possibility of converting the input string to the specified type.

    This function may not support most types.
    """
    result = False

    if expect_type is int:
        try:
            int(input_str)
            result = True
        except ValueError:
            result = False

    if expect_type is float:
        try:
            float(input_str)
            result = True
        except ValueError:
            result = False

    return result


class Validator:
    """Utility class to validate a request"""
    def __init__(self, req: request):
        self.req = req
        self.errors = []

    def format_errors(self, separator='\n'):
        """Format error messages with specified separator (default: `\\n`)."""
        return separator.join(self.errors)

    def check_form_param(self, name: str, expect_type: type = None) -> bool:
        """
        Validates the form parameter of specified name, and stores any error(s) into the Validator instance.

        Returns true if all validation steps have succeeded, otherwise returns false.
        """
        # check if the parameter exists
        if name not in self.req.form:
            self.errors.append(f"Missing parameter '{name}'")
            return False

        # check if the parameter is empty
        param = self.req.form[name]
        if not param:
            self.errors.append(f"Empty value for parameter '{name}'")
            return False

        # check if the parameter is of the correct value type
        if (expect_type is not None) and (not is_possible_type(param, expect_type)):
            self.errors.append(f"Unable to convert parameter '{name}' to '{expect_type.__name__}'")
            return False

        # if the code reaches here, then the parameter is valid
        return True

    def check_form_param_as_list(self, name: str, separator: str = ',', trim_elements: bool = False, expect_type: type = None) -> bool:
        """
        Validates the form parameter of specified name, and stores any error(s) into the Validator instance.

        This function also specifically validate that the parameter can be converted to a list of specified value type.
        """
        # basic check
        if self.check_form_param(name) is False:
            return False

        # check if the parameter can be converted to a list of specified value type
        param = self.req.form[name]
        list_param = param.split(separator)
        list_param = [e.strip() for e in list_param] if trim_elements is True else list_param

        has_element_errors = False

        if expect_type is not None:
            for i, element in enumerate(list_param):
                if not is_possible_type(element, expect_type):
                    self.errors.append(f"Unable to convert element #{i} of parameter '{name}' to '{expect_type.__name__}'")
                    has_element_errors = True

        if has_element_errors:
            return False

        # if the code reaches here, then the parameter is valid
        return True
