"""Vehicle API controller."""

from http import HTTPStatus

from flask import (
    Blueprint, jsonify, request, make_response, url_for
)
from werkzeug.exceptions import abort

from .common import should_get_all_attr, Validator
from ..db.db import db
from ..models.engine import Engine
from ..models.tire import Tire
from ..models.transmission import Transmission
from ..models.vehicle import Vehicle


blueprint = Blueprint('vehicle', __name__, url_prefix='/api/vehicle')


@blueprint.route('/', methods=('GET',))
def route_get_vehicles():
    """Retrieve all vehicle objects"""
    vehicles = Vehicle.query.all()

    dict_obj = [v.serialize_all if should_get_all_attr(request) else v.serialize for v in vehicles]

    return jsonify(dict_obj)


@blueprint.route('/', methods=('POST',))
def route_create_vehicle():
    """Create a new Vehicle object."""
    validator = Validator(request)
    validator.check_form_param('name')
    validator.check_form_param('brand')
    validator.check_form_param('color')
    validator.check_form_param('engine_id', expect_type=int)
    validator.check_form_param('transmission_id', expect_type=int)
    validator.check_form_param('tire_id', expect_type=int)

    engine_id = int(request.form['engine_id'])
    transmission_id = int(request.form['transmission_id'])
    tire_id = int(request.form['tire_id'])

    engine = Engine.query.get(engine_id)
    transmission = Transmission.query.get(transmission_id)
    tire = Tire.query.get(tire_id)

    if engine is None:
        validator.errors.append(f"Engine of id {engine_id} does not exist.")
    if transmission is None:
        validator.errors.append(f"Transmission of id {transmission_id} does not exist.")
    if tire is None:
        validator.errors.append(f"Tire of id {tire_id} does not exist.")

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    vehicle = Vehicle(
        name=request.form['name'],
        brand=request.form['brand'],
        color=request.form['color'])

    vehicle.engine_id = engine.id
    vehicle.transmission_id = transmission.id
    vehicle.tire_id = tire.id

    db.session.add(vehicle)
    db.session.commit()

    res = make_response(jsonify(vehicle.serialize), HTTPStatus.CREATED)
    res.headers['Location'] = url_for('vehicle.route_get_vehicle', v_id=vehicle.id)

    return res


@blueprint.route('/<int:v_id>', methods=('GET',))
def route_get_vehicle(v_id):
    """Retrieve a Vehicle object with specified ID."""
    vehicle = Vehicle.query.filter_by(id=v_id).first()

    if vehicle is None:
        abort(HTTPStatus.NOT_FOUND, f"Vehicle of id {v_id} does not exist.")

    dict_obj = vehicle.serialize_all if should_get_all_attr(request) else vehicle.serialize

    return jsonify(dict_obj)


@blueprint.route('/<int:v_id>/datasets', methods=('GET',))
def route_get_vehicle_datasets(v_id):
    """Retrieve datasets of a Vehicle object with specified ID."""
    vehicle = Vehicle.query.filter_by(id=v_id).first()

    if vehicle is None:
        abort(HTTPStatus.NOT_FOUND, f"Vehicle of id {v_id} does not exist.")

    list_obj = vehicle.extended_datasets if 'extended' in request.args else vehicle.datasets

    return jsonify(list_obj)


@blueprint.route('/<int:v_id>', methods=('PUT',))
def route_update_vehicle(v_id):
    """Update a Vehicle object with specified ID."""
    vehicle = Vehicle.query.get(v_id)

    if vehicle is None:
        abort(HTTPStatus.NOT_FOUND, f"Vehicle of id {v_id} does not exist.")

    validator = Validator(request)

    if 'name' in request.form and validator.check_form_param('name'):
        vehicle.name = request.form['name']
    if 'brand' in request.form and validator.check_form_param('brand'):
        vehicle.brand = request.form['brand']
    if 'color' in request.form and validator.check_form_param('color'):
        vehicle.color = request.form['color']

    if 'engine_id' in request.form and validator.check_form_param('engine_id', expect_type=int):
        engine_id = int(request.form['engine_id'])
        engine = Engine.query.get(engine_id)
        if engine is None:
            validator.errors.append(f"Engine of id {engine_id} does not exist.")
        else:
            vehicle.engine_id = engine.id
    if 'transmission_id' in request.form and validator.check_form_param('transmission_id', expect_type=int):
        transmission_id = int(request.form['transmission_id'])
        transmission = Transmission.query.get(transmission_id)
        if transmission is None:
            validator.errors.append(f"Transmission of id {transmission_id} does not exist.")
        else:
            vehicle.transmission_id = transmission.id
    if 'tire_id' in request.form and validator.check_form_param('tire_id', expect_type=int):
        tire_id = int(request.form['tire_id'])
        tire = Tire.query.get(tire_id)
        if tire is None:
            validator.errors.append(f"Tire of id {tire_id} does not exist.")
        else:
            vehicle.tire_id = tire.id

    if validator.errors:
        error_str = validator.format_errors()
        abort(HTTPStatus.UNPROCESSABLE_ENTITY, f'Input validation failed:\n{error_str}')

    db.session.commit()

    return make_response(jsonify(vehicle.serialize), HTTPStatus.NO_CONTENT)


@blueprint.route('/<int:v_id>', methods=('DELETE',))
def route_delete_vehicle(v_id):
    """Delete a Vehicle object with specified ID."""
    vehicle = Vehicle.query.get(v_id)

    if vehicle is None:
        abort(HTTPStatus.NOT_FOUND, f"Vehicle of id {v_id} does not exist.")

    db.session.delete(vehicle)
    db.session.commit()

    return ('', HTTPStatus.NO_CONTENT)
