# Powertrain Calculator

[![pipeline status](https://gitlab.com/MachHach/powertrain-calc/badges/master/pipeline.svg)](https://gitlab.com/MachHach/powertrain-calc/commits/master)
[![coverage report](https://gitlab.com/MachHach/powertrain-calc/badges/master/coverage.svg)](https://gitlab.com/MachHach/powertrain-calc/commits/master)

A web application that calculates and outputs the capability of a vehicle's powertrain.

## Getting started

```bash
pip install -r requirements.txt
flask init-db
flask fill-db
flask run
```

## Testing

```bash
coverage run -m pytest -v
coverage report
```

## License

MIT
