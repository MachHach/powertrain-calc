"""Main application."""

import logging
from logging.config import dictConfig

from colorama import (
    init as colorama_init, Fore, Back, Style
)


class ColoredLogHandler(logging.StreamHandler):
    """Custom log handler to output color based on log level of a log record"""
    colours = {
        'DEBUG': Fore.CYAN,
        'INFO': Fore.WHITE,
        'WARN': Fore.YELLOW,
        'WARNING': Fore.YELLOW,
        'ERROR': Fore.RED,
        'CRIT': Back.RED + Fore.WHITE,
        'CRITICAL': Back.RED + Fore.WHITE,
    }

    def emit(self, record):
        colour = Back.RESET + Fore.RESET
        try:
            colour = self.colours[record.levelname]
        except KeyError:
            pass
        try:
            message = self.format(record)
            self.stream.write(colour + message + Style.RESET_ALL)
            self.stream.write(getattr(self, 'terminator', '\n'))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


colorama_init(autoreset=True)


dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)-10s %(module)-15s %(message)s',
        }
    },
    'handlers': {
        'wsgi': {
            'class': 'app.ColoredLogHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }
    },
    'loggers': {
        '': {
            'handlers': ['wsgi'],
            'level': 'INFO',
        },
    }
})


def create_app(test_config=None):
    """
    Create an application instance.

    This function must be named `create_app` / `make_app` for the `flask run` command.
    """
    from powertrain_calc import app
    return app.create_app(test_config=test_config)


def main():
    """Main function."""
    create_app().run()


if __name__ == "__main__":
    main()
