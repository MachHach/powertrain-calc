"""Tests for Engine resource."""

from http import HTTPStatus

import pytest

from powertrain_calc.db.db import db
from powertrain_calc.models.engine import Engine


@pytest.fixture(autouse=True)
def setup_db(app):
    """Test fixture to initialize database with Engine objects."""
    with app.app_context():
        db.session.add(Engine(
            name="1NZ-FE",
            brand="Toyota",
            max_torque_rpm=4200,
            max_power_rpm=6000,
        ))
        db.session.add(Engine(
            name="2NR-FE",
            brand="Toyota",
            max_torque_rpm=4200,
            max_power_rpm=6000,
        ))
        db.session.commit()


def test_index(client):
    """Test viewing list of all Engine."""
    response = client.get('/api/engine/')
    assert response.status_code == HTTPStatus.OK
    assert len(response.get_json()) == 2


@pytest.mark.parametrize(('e_id', 'e_name', 'e_brand', 'e_max_torque_rpm', 'e_max_power_rpm'), (
    (1, '1NZ-FE', 'Toyota', 4200, 6000),
))
def test_view(client, e_id, e_name, e_brand, e_max_torque_rpm, e_max_power_rpm):
    """Test viewing an Engine."""
    response = client.get(f'/api/engine/{e_id}')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert json_data['id'] == e_id
    assert json_data['name'] == e_name
    assert json_data['brand'] == e_brand
    assert json_data['max_torque_rpm'] == e_max_torque_rpm
    assert json_data['max_power_rpm'] == e_max_power_rpm


@pytest.mark.parametrize(('e_id',), (
    (1,),
))
def test_view_derived(client, e_id):
    """Test viewing an Engine, including derived data."""
    response = client.get(f'/api/engine/{e_id}?mode=all')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert 'max_torque_rph' in json_data
    assert 'max_power_rph' in json_data


@pytest.mark.parametrize(('e_id', 'e_name', 'e_brand', 'e_max_torque_rpm', 'e_max_power_rpm'), (
    (3, '1JZ-GE', 'Toyota', 4000, 6000),
))
def test_create(client, app, e_id, e_name, e_brand, e_max_torque_rpm, e_max_power_rpm):
    """Test creating an Engine."""
    response = client.post('/api/engine/', data={
        'name': e_name,
        'brand': e_brand,
        'max_torque_rpm': e_max_torque_rpm,
        'max_power_rpm': e_max_power_rpm,
    })
    assert response.status_code == HTTPStatus.CREATED
    assert response.headers['Location'] == f'http://localhost/api/engine/{e_id}'

    json_data = response.get_json()
    assert json_data['id'] == e_id
    assert json_data['name'] == e_name
    assert json_data['brand'] == e_brand
    assert json_data['max_torque_rpm'] == e_max_torque_rpm
    assert json_data['max_power_rpm'] == e_max_power_rpm

    with app.app_context():
        engine = Engine.query.get(e_id)
        assert engine is not None
        assert engine.id == e_id
        assert engine.name == e_name
        assert engine.brand == e_brand
        assert engine.max_torque_rpm == e_max_torque_rpm
        assert engine.max_power_rpm == e_max_power_rpm


@pytest.mark.parametrize(('e_id', 'e_name', 'e_brand', 'e_max_torque_rpm', 'e_max_power_rpm'), (
    (2, 'B16B', 'Honda', 7500, 8200),
))
def test_update(client, app, e_id, e_name, e_brand, e_max_torque_rpm, e_max_power_rpm):
    """Test updating an Engine."""
    response = client.put(f'/api/engine/{e_id}', data={
        'name': e_name,
        'brand': e_brand,
        'max_torque_rpm': e_max_torque_rpm,
        'max_power_rpm': e_max_power_rpm,
    })
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        engine = Engine.query.get(e_id)
        assert engine is not None
        assert engine.id == e_id
        assert engine.name == e_name
        assert engine.brand == e_brand
        assert engine.max_torque_rpm == e_max_torque_rpm
        assert engine.max_power_rpm == e_max_power_rpm


@pytest.mark.parametrize('e_id', (1,))
def test_delete(client, app, e_id):
    """Test deleting an Engine."""
    response = client.delete(f'/api/engine/{e_id}')
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        engine = Engine.query.get(e_id)
        assert engine is None
