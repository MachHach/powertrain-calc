"""Tests for commands."""

def test_init_db(runner, monkeypatch):
    """Test running command to initialize database."""
    # mock "flask init-db" since the `app` fixture has already initialized the database
    class Recorder:
        """Class to record some state."""
        called = False

    def mock_init_db():
        """Function to mock another function."""
        Recorder.called = True

    monkeypatch.setattr('powertrain_calc.db.db.init_db', mock_init_db)
    result = runner.invoke(args=['init-db'])

    assert 'Initialized the database' in result.output
    assert Recorder.called


def test_fill_db(runner):
    """Test running command to populate database with sample data."""
    result = runner.invoke(args=['fill-db'])

    assert 'Filled the database' in result.output
