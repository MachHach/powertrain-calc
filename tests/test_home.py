"""Tests for home page."""

from http import HTTPStatus

import pytest

from powertrain_calc.models.models import fill_db


@pytest.fixture(autouse=True)
def setup_db(app):
    """Test fixture to initialize database."""
    with app.app_context():
        fill_db()


def test_index(client):
    """Test viewing home page."""
    response = client.get('/')
    assert response.status_code == HTTPStatus.OK
    assert b'<title>Powertrain Calculator</title>' in response.data
