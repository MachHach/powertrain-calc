"""Test fixtures for `pytest`."""

import os
import tempfile

import pytest

from powertrain_calc.app import create_app
from powertrain_calc.db.db import init_db


@pytest.fixture
def app():
    """Test fixture for Flask app object."""
    db_fd, db_path = tempfile.mkstemp()
    normalized_path = db_path.replace('\\', '\\\\')

    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': f'sqlite:///{normalized_path}'
    })

    with app.app_context():
        init_db()

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    """Test fixture for web client."""
    return app.test_client()


@pytest.fixture
def runner(app):
    """Test fixture for CLI."""
    return app.test_cli_runner()
