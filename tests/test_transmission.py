"""Tests for Transmission resource."""

from http import HTTPStatus

import pytest

from powertrain_calc.db.db import db
from powertrain_calc.models.transmission import Transmission, TransmissionRatio


@pytest.fixture(autouse=True)
def setup_db(app):
    """Test fixture to initialize database with Transmission objects."""
    with app.app_context():
        db.session.add(Transmission(
            name="U340E",
            brand="Toyota",
            reverse_ratio=2.343,
            final_drive_ratio=3.924,
            forward_ratios=[TransmissionRatio(gear_order=i+1, forward_ratio=r) for i, r in enumerate([2.847, 1.552, 1.000, 0.700])]
        ))
        db.session.add(Transmission(
            name="C150",
            brand="Toyota",
            reverse_ratio=3.250,
            final_drive_ratio=4.058,
            forward_ratios=[TransmissionRatio(gear_order=i+1, forward_ratio=r) for i, r in enumerate([3.545, 1.904, 1.310, 0.969, 0.815])]
        ))
        db.session.commit()


def test_index(client):
    """Test viewing list of all Transmission."""
    response = client.get('/api/transmission/')
    assert response.status_code == HTTPStatus.OK
    assert len(response.get_json()) == 2


@pytest.mark.parametrize(('t_id', 't_name', 't_brand', 't_reverse_ratio', 't_final_drive_ratio'), (
    (1, 'U340E', 'Toyota', 2.343, 3.924),
))
def test_view(client, t_id, t_name, t_brand, t_reverse_ratio, t_final_drive_ratio):
    """Test viewing a Transmission."""
    response = client.get(f'/api/transmission/{t_id}')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert json_data['id'] == t_id
    assert json_data['name'] == t_name
    assert json_data['brand'] == t_brand
    assert json_data['reverse_ratio'] == t_reverse_ratio
    assert json_data['final_drive_ratio'] == t_final_drive_ratio


@pytest.mark.parametrize(('t_id',), (
    (1,),
))
def test_view_derived(client, t_id):
    """Test viewing a Transmission, including derived data."""
    response = client.get(f'/api/transmission/{t_id}?mode=all')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert 'initial_forward_ratios' in json_data
    assert 'final_forward_ratios' in json_data
    assert 'final_reverse_ratio' in json_data


@pytest.mark.parametrize(('t_id', 't_name', 't_brand', 't_reverse_ratio', 't_final_drive_ratio', 't_forward_ratios'), (
    (3, 'A960E', 'Toyota', 3.168, 4.100, [3.538, 2.060, 1.404, 1.000, 0.713, 0.582]),
))
def test_create(client, app, t_id, t_name, t_brand, t_reverse_ratio, t_final_drive_ratio, t_forward_ratios):
    """Test creating a Transmission."""
    response = client.post('/api/transmission/', data={
        'name': t_name,
        'brand': t_brand,
        'reverse_ratio': t_reverse_ratio,
        'final_drive_ratio': t_final_drive_ratio,
        'forward_ratios': ','.join(map(str, t_forward_ratios)),
    })
    assert response.status_code == HTTPStatus.CREATED
    assert response.headers['Location'] == f'http://localhost/api/transmission/{t_id}'

    json_data = response.get_json()
    assert json_data['id'] == t_id
    assert json_data['name'] == t_name
    assert json_data['brand'] == t_brand
    assert json_data['reverse_ratio'] == t_reverse_ratio
    assert json_data['final_drive_ratio'] == t_final_drive_ratio

    with app.app_context():
        transmission = Transmission.query.get(t_id)
        assert transmission is not None
        assert transmission.id == t_id
        assert transmission.name == t_name
        assert transmission.brand == t_brand
        assert transmission.reverse_ratio == t_reverse_ratio
        assert transmission.final_drive_ratio == t_final_drive_ratio

        transmission_ratios = TransmissionRatio.query.filter_by(transmission_id=t_id).all()
        assert transmission_ratios is not None

        forward_ratios = [r.forward_ratio for r in transmission_ratios]
        for expected_ratio in t_forward_ratios:
            assert expected_ratio in forward_ratios


@pytest.mark.parametrize(('t_id', 't_name', 't_brand', 't_reverse_ratio', 't_final_drive_ratio', 't_forward_ratios'), (
    (2, '6MT', 'Honda', 3.758, 4.111, [3.625, 2.115, 1.529, 1.125, 0.911, 0.735]),
))
def test_update(client, app, t_id, t_name, t_brand, t_reverse_ratio, t_final_drive_ratio, t_forward_ratios):
    """Test updating a Transmission."""
    response = client.put(f'/api/transmission/{t_id}', data={
        'name': t_name,
        'brand': t_brand,
        'reverse_ratio': t_reverse_ratio,
        'final_drive_ratio': t_final_drive_ratio,
        'forward_ratios': ','.join(map(str, t_forward_ratios)),
    })
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        transmission = Transmission.query.get(t_id)
        assert transmission is not None
        assert transmission.id == t_id
        assert transmission.name == t_name
        assert transmission.brand == t_brand
        assert transmission.reverse_ratio == t_reverse_ratio
        assert transmission.final_drive_ratio == t_final_drive_ratio

        transmission_ratios = TransmissionRatio.query.filter_by(transmission_id=t_id).all()
        assert transmission_ratios is not None

        forward_ratios = [r.forward_ratio for r in transmission_ratios]
        for expected_ratio in t_forward_ratios:
            assert expected_ratio in forward_ratios


@pytest.mark.parametrize('t_id', (1,))
def test_delete(client, app, t_id):
    """Test deleting a Transmission."""
    response = client.delete(f'/api/transmission/{t_id}')
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        transmission = Transmission.query.get(t_id)
        assert transmission is None

        transmission_ratios = TransmissionRatio.query.filter_by(transmission_id=t_id).all()
        assert not transmission_ratios
