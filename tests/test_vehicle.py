"""Tests for Vehicle resource."""

from http import HTTPStatus

import pytest

from powertrain_calc.models.models import fill_db
from powertrain_calc.models.vehicle import Vehicle


@pytest.fixture(autouse=True)
def setup_db(app):
    """Test fixture to initialize database with Vehicle objects."""
    with app.app_context():
        fill_db()


def test_index(client):
    """Test viewing list of all Vehicle."""
    response = client.get('/api/vehicle/')
    assert response.status_code == HTTPStatus.OK
    assert len(response.get_json()) == 6


@pytest.mark.parametrize(('v_id', 'v_name', 'v_brand', 'v_color'), (
    (1, 'Vios 1.5G AT (NCP42)', 'Toyota', 'rgba(120, 120, 120, 1)'),
))
def test_view(client, v_id, v_name, v_brand, v_color):
    """Test viewing a Vehicle."""
    response = client.get(f'/api/vehicle/{v_id}')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert json_data['id'] == v_id
    assert json_data['name'] == v_name
    assert json_data['brand'] == v_brand
    assert json_data['color'] == v_color


@pytest.mark.parametrize(('v_id',), (
    (1,),
))
def test_view_derived(client, v_id):
    """Test viewing a Vehicle, including derived data."""
    response = client.get(f'/api/vehicle/{v_id}?mode=all')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert 'engine_id' in json_data
    assert 'transmission_id' in json_data
    assert 'tire_id' in json_data
    assert 'max_torque_speeds_kph' in json_data
    assert 'max_power_speeds_kph' in json_data


@pytest.mark.parametrize(('v_id',), (
    (1,),
))
def test_view_datasets(client, v_id):
    """Test viewing a Vehicle's datasets."""
    response = client.get(f'/api/vehicle/{v_id}/datasets')
    assert response.status_code == HTTPStatus.OK


@pytest.mark.parametrize(('v_id',), (
    (1,),
))
def test_view_datasets_extended(client, v_id):
    """Test viewing a Vehicle's datasets with extended data."""
    response = client.get(f'/api/vehicle/{v_id}/datasets?extended')
    assert response.status_code == HTTPStatus.OK


@pytest.mark.parametrize(('v_id', 'v_name', 'v_brand', 'v_color', 'v_engine_id', 'v_trans_id', 'v_tire_id'), (
    (7, 'Vios 1.5E AT (NCP93)', 'Toyota', 'rgba(90, 90, 90, 1)', 1, 1, 2),
))
def test_create(client, app, v_id, v_name, v_brand, v_color, v_engine_id, v_trans_id, v_tire_id):
    """Test creating a Vehicle."""
    response = client.post('/api/vehicle/', data={
        'name': v_name,
        'brand': v_brand,
        'color': v_color,
        'engine_id': v_engine_id,
        'transmission_id': v_trans_id,
        'tire_id': v_tire_id,
    })
    assert response.status_code == HTTPStatus.CREATED
    assert response.headers['Location'] == f'http://localhost/api/vehicle/{v_id}'

    json_data = response.get_json()
    assert json_data['id'] == v_id
    assert json_data['name'] == v_name
    assert json_data['brand'] == v_brand
    assert json_data['color'] == v_color

    with app.app_context():
        vehicle = Vehicle.query.get(v_id)
        assert vehicle is not None
        assert vehicle.id == v_id
        assert vehicle.name == v_name
        assert vehicle.brand == v_brand
        assert vehicle.color == v_color
        assert vehicle.engine.id == v_engine_id
        assert vehicle.transmission.id == v_trans_id
        assert vehicle.tire.id == v_tire_id


@pytest.mark.parametrize(('v_id', 'v_name', 'v_brand', 'v_color', 'v_engine_id', 'v_trans_id', 'v_tire_id'), (
    (2, 'Civic 1.8 AT (FB)', 'Honda', 'rgba(200, 200, 200, 1)', 3, 3, 3),
))
def test_update(client, app, v_id, v_name, v_brand, v_color, v_engine_id, v_trans_id, v_tire_id):
    """Test updating a Vehicle."""
    response = client.put(f'/api/vehicle/{v_id}', data={
        'name': v_name,
        'brand': v_brand,
        'color': v_color,
        'engine_id': v_engine_id,
        'transmission_id': v_trans_id,
        'tire_id': v_tire_id,
    })
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        vehicle = Vehicle.query.get(v_id)
        assert vehicle is not None
        assert vehicle.id == v_id
        assert vehicle.name == v_name
        assert vehicle.brand == v_brand
        assert vehicle.color == v_color
        assert vehicle.engine.id == v_engine_id
        assert vehicle.transmission.id == v_trans_id
        assert vehicle.tire.id == v_tire_id


@pytest.mark.parametrize('v_id', (1,))
def test_delete(client, app, v_id):
    """Test deleting a Vehicle."""
    response = client.delete(f'/api/vehicle/{v_id}')
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        vehicle = Vehicle.query.get(v_id)
        assert vehicle is None
