"""Tests for Tire resource."""

from http import HTTPStatus

import pytest

from powertrain_calc.db.db import db
from powertrain_calc.models.tire import Tire


@pytest.fixture(autouse=True)
def setup_db(app):
    """Test fixture to initialize database with Tire objects."""
    with app.app_context():
        db.session.add(Tire(
            width=195,
            aspect_ratio=55,
            wheel_radius=15,
        ))
        db.session.add(Tire(
            width=185,
            aspect_ratio=60,
            wheel_radius=15,
        ))
        db.session.commit()


def test_index(client):
    """Test viewing list of all Tire."""
    response = client.get('/api/tire/')
    assert response.status_code == HTTPStatus.OK
    assert len(response.get_json()) == 2


@pytest.mark.parametrize(('t_id', 't_width', 't_aspect_ratio', 't_wheel_radius'), (
    (1, 195, 55, 15),
))
def test_view(client, t_id, t_width, t_aspect_ratio, t_wheel_radius):
    """Test viewing a Tire."""
    response = client.get(f'/api/tire/{t_id}')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert json_data['id'] == t_id
    assert json_data['width'] == t_width
    assert json_data['aspect_ratio'] == t_aspect_ratio
    assert json_data['wheel_radius'] == t_wheel_radius


@pytest.mark.parametrize(('t_id',), (
    (1,),
))
def test_view_derived(client, t_id):
    """Test viewing a Tire, including derived data."""
    response = client.get(f'/api/tire/{t_id}?mode=all')
    assert response.status_code == HTTPStatus.OK

    json_data = response.get_json()
    assert 'aspect_ratio_fraction' in json_data
    assert 'wheel_radius_mm' in json_data
    assert 'sidewall_height_mm' in json_data
    assert 'diameter_mm' in json_data
    assert 'circumference_mm' in json_data
    assert 'circumference_km' in json_data


@pytest.mark.parametrize(('t_id', 't_width', 't_aspect_ratio', 't_wheel_radius'), (
    (3, 215, 45, 17),
))
def test_create(client, app, t_id, t_width, t_aspect_ratio, t_wheel_radius):
    """Test creating a Tire."""
    response = client.post('/api/tire/', data={
        'width': t_width,
        'aspect_ratio': t_aspect_ratio,
        'wheel_radius': t_wheel_radius,
    })
    assert response.status_code == HTTPStatus.CREATED
    assert response.headers['Location'] == f'http://localhost/api/tire/{t_id}'

    json_data = response.get_json()
    assert json_data['id'] == t_id
    assert json_data['width'] == t_width
    assert json_data['aspect_ratio'] == t_aspect_ratio
    assert json_data['wheel_radius'] == t_wheel_radius

    with app.app_context():
        tire = Tire.query.get(t_id)
        assert tire is not None
        assert tire.id == t_id
        assert tire.width == t_width
        assert tire.aspect_ratio == t_aspect_ratio
        assert tire.wheel_radius == t_wheel_radius


@pytest.mark.parametrize(('t_id', 't_width', 't_aspect_ratio', 't_wheel_radius'), (
    (2, 245, 30, 20),
))
def test_update(client, app, t_id, t_width, t_aspect_ratio, t_wheel_radius):
    """Test updating a Tire."""
    response = client.put(f'/api/tire/{t_id}', data={
        'width': t_width,
        'aspect_ratio': t_aspect_ratio,
        'wheel_radius': t_wheel_radius,
    })
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        tire = Tire.query.get(t_id)
        assert tire is not None
        assert tire.id == t_id
        assert tire.width == t_width
        assert tire.aspect_ratio == t_aspect_ratio
        assert tire.wheel_radius == t_wheel_radius


@pytest.mark.parametrize('t_id', (1,))
def test_delete(client, app, t_id):
    """Test deleting a Tire."""
    response = client.delete(f'/api/tire/{t_id}')
    assert response.status_code == HTTPStatus.NO_CONTENT

    with app.app_context():
        tire = Tire.query.get(t_id)
        assert tire is None
